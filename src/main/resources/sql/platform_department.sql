/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : hospital_114

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 15/04/2022 11:04:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for platform_department
-- ----------------------------
DROP TABLE IF EXISTS `platform_department`;
CREATE TABLE `platform_department`  (
  `id` int NOT NULL COMMENT '科室表唯一标识',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '科室名称',
  `status` tinyint NOT NULL COMMENT '科室状态0：禁用，1：启用',
  `pre_depart_id` int NOT NULL COMMENT '上级科室id',
  `hospital_id` int NOT NULL COMMENT '医院ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'platform_department' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of platform_department
-- -----------------------INSERT INTO `platform_department` VALUES (1, '核酸检测', 1, 0, 0);
INSERT INTO `platform_department` VALUES (2, '全科医疗科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (3, '内科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (4, '外科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (5, '重症医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (6, '急症介入科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (7, '肿瘤科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (8, '妇产科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (9, '儿科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (10, '小儿外科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (11, '眼科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (12, '耳鼻喉头颈外科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (13, '口腔科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (14, '皮肤科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (15, '医疗美容科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (17, '精神科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (18, '结核病科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (19, '康复医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (20, '运动医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (21, '职业病科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (22, '民族医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (23, '疼痛科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (24, '心身医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (25, '理疗科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (26, '简易门诊', 1, 0, 0);
INSERT INTO `platform_department` VALUES (27, '传染科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (28, '中医科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (29, '老年医学科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (30, '急诊科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (31, '感染病科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (32, '多学科联合门诊', 1, 0, 0);
INSERT INTO `platform_department` VALUES (33, '医学影像科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (34, '病理科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (35, '营养科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (36, '医学检验科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (37, '麻醉科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (38, '药剂科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (39, '心理科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (40, '体检中心', 1, 0, 0);
INSERT INTO `platform_department` VALUES (41, '介入科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (42, '透析科', 1, 0, 0);
INSERT INTO `platform_department` VALUES (43, '核磁', 1, 0, 0);
INSERT INTO `platform_department` VALUES (44, '核酸检测', 1, 1, 1);
INSERT INTO `platform_department` VALUES (44, '核酸检测', 1, 1, 2);
INSERT INTO `platform_department` VALUES (44, '核酸检测', 1, 1, 3);
INSERT INTO `platform_department` VALUES (44, '核酸检测', 1, 1, 4);
INSERT INTO `platform_department` VALUES (45, '全科医疗科', 1, 2, 1);
INSERT INTO `platform_department` VALUES (45, '全科医疗科', 1, 2, 2);
INSERT INTO `platform_department` VALUES (45, '全科医疗科', 1, 2, 3);
INSERT INTO `platform_department` VALUES (45, '全科医疗科', 1, 2, 4);
INSERT INTO `platform_department` VALUES (46, '流动医院', 1, 2, 1);
INSERT INTO `platform_department` VALUES (46, '流动医院', 1, 2, 2);
INSERT INTO `platform_department` VALUES (47, '离休门诊', 1, 2, 1);
INSERT INTO `platform_department` VALUES (48, '特约门诊', 1, 2, 1);
INSERT INTO `platform_department` VALUES (48, '特约门诊', 1, 2, 2);
INSERT INTO `platform_department` VALUES (49, '内科', 1, 3, 1);
INSERT INTO `platform_department` VALUES (49, '内科', 1, 3, 2);
INSERT INTO `platform_department` VALUES (49, '内科', 1, 3, 3);
INSERT INTO `platform_department` VALUES (49, '内科', 1, 3, 4);
INSERT INTO `platform_department` VALUES (50, '呼吸内科专业', 1, 3, 1);
INSERT INTO `platform_department` VALUES (50, '呼吸内科专业', 1, 3, 2);
INSERT INTO `platform_department` VALUES (50, '呼吸内科专业', 1, 3, 3);
INSERT INTO `platform_department` VALUES (50, '呼吸内科专业', 1, 3, 4);
INSERT INTO `platform_department` VALUES (51, '呼吸康复专业', 1, 3, 1);
INSERT INTO `platform_department` VALUES (51, '呼吸康复专业', 1, 3, 2);
INSERT INTO `platform_department` VALUES (51, '呼吸康复专业', 1, 3, 3);
INSERT INTO `platform_department` VALUES (51, '呼吸康复专业', 1, 3, 4);
INSERT INTO `platform_department` VALUES (52, '哮喘', 1, 3, 1);
INSERT INTO `platform_department` VALUES (52, '哮喘', 1, 3, 2);
INSERT INTO `platform_department` VALUES (52, '哮喘', 1, 3, 3);
INSERT INTO `platform_department` VALUES (52, '哮喘', 1, 3, 4);
INSERT INTO `platform_department` VALUES (53, '打鼾', 1, 3, 3);
INSERT INTO `platform_department` VALUES (53, '打鼾', 1, 3, 4);
INSERT INTO `platform_department` VALUES (54, '戒烟', 1, 3, 0);
INSERT INTO `platform_department` VALUES (55, '睡眠', 1, 3, 3);
INSERT INTO `platform_department` VALUES (56, '肺病专业', 1, 3, 1);
INSERT INTO `platform_department` VALUES (56, '肺病专业', 1, 3, 2);
INSERT INTO `platform_department` VALUES (57, '呼吸道感染', 1, 3, 1);
INSERT INTO `platform_department` VALUES (57, '呼吸道感染', 1, 3, 2);
INSERT INTO `platform_department` VALUES (57, '呼吸道感染', 1, 3, 3);
INSERT INTO `platform_department` VALUES (57, '呼吸道感染', 1, 3, 4);
INSERT INTO `platform_department` VALUES (58, '消化内科专业', 1, 3, 2);
INSERT INTO `platform_department` VALUES (58, '消化内科专业', 1, 3, 3);
INSERT INTO `platform_department` VALUES (58, '消化内科专业', 1, 3, 4);
INSERT INTO `platform_department` VALUES (59, '神经内科专业', 1, 3, 1);
INSERT INTO `platform_department` VALUES (59, '神经内科专业', 1, 3, 2);
INSERT INTO `platform_department` VALUES (59, '神经内科专业', 1, 3, 3);
INSERT INTO `platform_department` VALUES (60, '外科', 1, 4, 1);
INSERT INTO `platform_department` VALUES (60, '外科', 1, 4, 2);
INSERT INTO `platform_department` VALUES (60, '外科', 1, 4, 3);
INSERT INTO `platform_department` VALUES (60, '外科', 1, 4, 4);
INSERT INTO `platform_department` VALUES (61, '神经外科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (61, '神经外科专业', 1, 4, 2);
INSERT INTO `platform_department` VALUES (61, '神经外科专业', 1, 4, 3);
INSERT INTO `platform_department` VALUES (62, '神经介入学', 1, 4, 3);
INSERT INTO `platform_department` VALUES (62, '神经介入学', 1, 4, 4);
INSERT INTO `platform_department` VALUES (63, '脊柱神经外科', 1, 4, 1);
INSERT INTO `platform_department` VALUES (63, '脊柱神经外科', 1, 4, 2);
INSERT INTO `platform_department` VALUES (64, '功能神经外科', 1, 4, 1);
INSERT INTO `platform_department` VALUES (64, '功能神经外科', 1, 4, 2);
INSERT INTO `platform_department` VALUES (65, '普通外科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (65, '普通外科专业', 1, 4, 2);
INSERT INTO `platform_department` VALUES (65, '普通外科专业', 1, 4, 3);
INSERT INTO `platform_department` VALUES (65, '普通外科专业', 1, 4, 4);
INSERT INTO `platform_department` VALUES (66, '胆囊', 1, 4, 1);
INSERT INTO `platform_department` VALUES (66, '胆囊', 1, 4, 2);
INSERT INTO `platform_department` VALUES (66, '胆囊', 1, 4, 3);
INSERT INTO `platform_department` VALUES (67, '肝脏', 1, 4, 1);
INSERT INTO `platform_department` VALUES (67, '肝脏', 1, 4, 2);
INSERT INTO `platform_department` VALUES (67, '肝脏', 1, 4, 3);
INSERT INTO `platform_department` VALUES (68, '胰腺', 1, 4, 1);
INSERT INTO `platform_department` VALUES (68, '胰腺', 1, 4, 2);
INSERT INTO `platform_department` VALUES (68, '肛肠', 1, 4, 3);
INSERT INTO `platform_department` VALUES (68, '肛肠', 1, 4, 4);
INSERT INTO `platform_department` VALUES (69, '便秘', 1, 4, 4);
INSERT INTO `platform_department` VALUES (70, '心脏外科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (70, '心脏外科专业', 1, 4, 2);
INSERT INTO `platform_department` VALUES (70, '心脏外科专业', 1, 4, 3);
INSERT INTO `platform_department` VALUES (70, '心脏外科专业', 1, 4, 4);
INSERT INTO `platform_department` VALUES (71, '心脏移植', 1, 4, 1);
INSERT INTO `platform_department` VALUES (72, '骨科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (72, '骨科专业', 1, 4, 2);
INSERT INTO `platform_department` VALUES (72, '骨科专业', 1, 4, 3);
INSERT INTO `platform_department` VALUES (72, '骨科专业', 1, 4, 4);
INSERT INTO `platform_department` VALUES (73, '关节外科', 1, 4, 2);
INSERT INTO `platform_department` VALUES (73, '关节外科', 1, 4, 4);
INSERT INTO `platform_department` VALUES (74, '骨质疏松', 1, 4, 1);
INSERT INTO `platform_department` VALUES (74, '骨质疏松', 1, 4, 4);
INSERT INTO `platform_department` VALUES (75, '胸外科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (75, '胸外科专业', 1, 4, 3);
INSERT INTO `platform_department` VALUES (75, '胸外科专业', 1, 4, 4);
INSERT INTO `platform_department` VALUES (76, '脑外科专业', 1, 4, 1);
INSERT INTO `platform_department` VALUES (77, '重症医学科', 1, 5, 1);
INSERT INTO `platform_department` VALUES (77, '重症医学科', 1, 5, 2);
INSERT INTO `platform_department` VALUES (77, '重症医学科', 1, 5, 3);
INSERT INTO `platform_department` VALUES (77, '重症医学科', 1, 5, 4);
INSERT INTO `platform_department` VALUES (78, '急症介入科', 1, 6, 1);
INSERT INTO `platform_department` VALUES (78, '急症介入科', 1, 6, 4);
INSERT INTO `platform_department` VALUES (79, '肿瘤科', 1, 7, 1);
INSERT INTO `platform_department` VALUES (79, '肿瘤科', 1, 7, 2);
INSERT INTO `platform_department` VALUES (79, '肿瘤科', 1, 7, 3);
INSERT INTO `platform_department` VALUES (79, '肿瘤科', 1, 7, 4);
INSERT INTO `platform_department` VALUES (80, '肿瘤内科', 1, 7, 1);
INSERT INTO `platform_department` VALUES (80, '肿瘤内科', 1, 7, 2);
INSERT INTO `platform_department` VALUES (80, '肿瘤内科', 1, 7, 3);
INSERT INTO `platform_department` VALUES (80, '肿瘤内科', 1, 7, 4);
INSERT INTO `platform_department` VALUES (81, '肿瘤外科', 1, 7, 1);
INSERT INTO `platform_department` VALUES (81, '肿瘤外科', 1, 7, 2);
INSERT INTO `platform_department` VALUES (81, '肿瘤外科', 1, 7, 3);
INSERT INTO `platform_department` VALUES (82, '化疗门诊', 1, 7, 1);
INSERT INTO `platform_department` VALUES (82, '化疗门诊', 1, 7, 2);
INSERT INTO `platform_department` VALUES (82, '化疗门诊', 1, 7, 3);
INSERT INTO `platform_department` VALUES (82, '化疗门诊', 1, 7, 4);
INSERT INTO `platform_department` VALUES (83, '妇产科', 1, 8, 1);
INSERT INTO `platform_department` VALUES (83, '妇产科', 1, 8, 2);
INSERT INTO `platform_department` VALUES (83, '妇产科', 1, 8, 3);
INSERT INTO `platform_department` VALUES (83, '妇产科', 1, 8, 4);
INSERT INTO `platform_department` VALUES (84, '妇科', 1, 8, 1);
INSERT INTO `platform_department` VALUES (84, '妇科', 1, 8, 2);
INSERT INTO `platform_department` VALUES (84, '妇科', 1, 8, 3);
INSERT INTO `platform_department` VALUES (84, '妇科', 1, 8, 4);
INSERT INTO `platform_department` VALUES (85, '产科', 1, 8, 3);
INSERT INTO `platform_department` VALUES (85, '产科', 1, 8, 4);
INSERT INTO `platform_department` VALUES (86, '儿科', 1, 9, 1);
INSERT INTO `platform_department` VALUES (86, '儿科', 1, 9, 2);
INSERT INTO `platform_department` VALUES (86, '儿科', 1, 9, 3);
INSERT INTO `platform_department` VALUES (86, '儿科', 1, 9, 4);
INSERT INTO `platform_department` VALUES (87, '小儿传染病专业', 1, 9, 1);
INSERT INTO `platform_department` VALUES (87, '小儿传染病专业', 1, 9, 3);
INSERT INTO `platform_department` VALUES (87, '小儿传染病专业', 1, 9, 4);
INSERT INTO `platform_department` VALUES (88, '小儿心脏病专业', 1, 9, 1);
INSERT INTO `platform_department` VALUES (88, '小儿心脏病专业', 1, 9, 2);
INSERT INTO `platform_department` VALUES (88, '小儿心脏病专业', 1, 9, 3);


SET FOREIGN_KEY_CHECKS = 1;
