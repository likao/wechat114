/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : hospital_114

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 15/04/2022 11:36:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '科室表唯一标识',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '科室名称',
  `status` tinyint NOT NULL COMMENT '科室状态0：禁用，1：启用',
  `pre_depart_id` int NOT NULL COMMENT '上级科室id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'department' ROW_FORMAT = Dynamic;


-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (1, '内科系统', 1, 0);
INSERT INTO `department` VALUES (2, '外科系统', 1, 0);
INSERT INTO `department` VALUES (3, '妇儿', 1, 0);
INSERT INTO `department` VALUES (4, '五官科', 1, 0);
INSERT INTO `department` VALUES (5, '全科医学科', 1, 0);
INSERT INTO `department` VALUES (6, '康复医学科', 1, 0);
INSERT INTO `department` VALUES (7, '其他科室', 1, 0);
INSERT INTO `department` VALUES (8, '男科', 1, 0);
INSERT INTO `department` VALUES (9, '呼吸内科门诊', 1, 1);
INSERT INTO `department` VALUES (10, '消化内科门诊', 1, 1);
INSERT INTO `department` VALUES (11, '心血管门诊', 1, 1);
INSERT INTO `department` VALUES (12, '内分泌内科门诊', 1, 1);
INSERT INTO `department` VALUES (13, '血液门诊', 1, 1);
INSERT INTO `department` VALUES (14, '肾脏门诊', 1, 1);
INSERT INTO `department` VALUES (15, '风湿免疫门诊', 1, 1);
INSERT INTO `department` VALUES (16, '神经内科门诊', 1, 1);
INSERT INTO `department` VALUES (17, '感染科门诊', 1, 1);
INSERT INTO `department` VALUES (18, '中医中西医门诊', 1, 1);
INSERT INTO `department` VALUES (19, '抗感染门诊', 1, 1);
INSERT INTO `department` VALUES (20, '心理门诊', 1, 1);
INSERT INTO `department` VALUES (21, '外科肿瘤门诊', 1, 2);
INSERT INTO `department` VALUES (22, '整形烧伤门诊', 1, 2);
INSERT INTO `department` VALUES (23, '骨科门诊', 1, 2);
INSERT INTO `department` VALUES (24, '胸外科门诊', 1, 2);
INSERT INTO `department` VALUES (25, '泌尿外科门诊', 1, 2);
INSERT INTO `department` VALUES (26, '麻醉科术前评估', 1, 2);
INSERT INTO `department` VALUES (27, '疼痛门诊', 1, 2);
INSERT INTO `department` VALUES (28, '介入血管外科门诊', 1, 2);
INSERT INTO `department` VALUES (29, '神经外科门诊', 1, 2);
INSERT INTO `department` VALUES (30, '放疗门诊', 1, 2);
INSERT INTO `department` VALUES (31, '化疗门诊', 1, 2);
INSERT INTO `department` VALUES (32, '心外科门诊', 1, 2);
INSERT INTO `department` VALUES (33, '普外科门诊', 1, 2);
INSERT INTO `department` VALUES (34, '儿科门诊', 1, 3);
INSERT INTO `department` VALUES (35, '麻醉术前评估（妇儿）', 1, 3);
INSERT INTO `department` VALUES (36, '生殖与遗传中心', 1, 3);
INSERT INTO `department` VALUES (37, '小儿外科门诊', 1, 3);
INSERT INTO `department` VALUES (38, '儿童癫痫中心门诊', 1, 3);
INSERT INTO `department` VALUES (39, '耳鼻喉科（儿童）门诊', 1, 3);
INSERT INTO `department` VALUES (40, '康复医学妇儿', 1, 3);
INSERT INTO `department` VALUES (41, '产科门诊', 1, 3);
INSERT INTO `department` VALUES (42, '计划生育门诊', 1, 3);
INSERT INTO `department` VALUES (43, '妇科门诊', 1, 3);
INSERT INTO `department` VALUES (44, '眼科门诊', 1, 4);
INSERT INTO `department` VALUES (45, '视光门诊', 1, 4);
INSERT INTO `department` VALUES (46, '小儿眼科门诊', 1, 4);
INSERT INTO `department` VALUES (47, '皮肤科门诊', 1, 4);
INSERT INTO `department` VALUES (48, '口腔科门诊', 1, 4);
INSERT INTO `department` VALUES (49, '耳鼻喉科门诊', 1, 4);
INSERT INTO `department` VALUES (50, '全科医学科门诊', 1, 5);
INSERT INTO `department` VALUES (51, '康复医学科门诊', 1, 6);
INSERT INTO `department` VALUES (52, '内镜中心门诊', 1, 7);
INSERT INTO `department` VALUES (53, '临床营养科门诊', 1, 7);
INSERT INTO `department` VALUES (54, '核医学科门诊', 1, 7);
INSERT INTO `department` VALUES (55, '医学影像科门诊', 1, 7);
INSERT INTO `department` VALUES (56, '泌四门诊', 1, 8);
SET FOREIGN_KEY_CHECKS = 1;
