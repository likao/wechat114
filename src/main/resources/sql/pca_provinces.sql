/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : hospital_114

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 15/04/2022 14:08:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pca_provinces
-- ----------------------------
DROP TABLE IF EXISTS `pca_provinces`;
CREATE TABLE `pca_provinces`  (
  `province_id` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '省编号',
  `province_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '省名称',
  PRIMARY KEY (`province_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '省份表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of pca_provinces
-- ----------------------------
INSERT INTO `pca_provinces` VALUES ('110000', '北京市');
INSERT INTO `pca_provinces` VALUES ('120000', '天津市');
INSERT INTO `pca_provinces` VALUES ('130000', '河北省');
INSERT INTO `pca_provinces` VALUES ('140000', '山西省');
INSERT INTO `pca_provinces` VALUES ('150000', '内蒙古自治区');
INSERT INTO `pca_provinces` VALUES ('210000', '辽宁省');
INSERT INTO `pca_provinces` VALUES ('220000', '吉林省');
INSERT INTO `pca_provinces` VALUES ('230000', '黑龙江省');
INSERT INTO `pca_provinces` VALUES ('310000', '上海市');
INSERT INTO `pca_provinces` VALUES ('320000', '江苏省');
INSERT INTO `pca_provinces` VALUES ('330000', '浙江省');
INSERT INTO `pca_provinces` VALUES ('340000', '安徽省');
INSERT INTO `pca_provinces` VALUES ('350000', '福建省');
INSERT INTO `pca_provinces` VALUES ('360000', '江西省');
INSERT INTO `pca_provinces` VALUES ('370000', '山东省');
INSERT INTO `pca_provinces` VALUES ('410000', '河南省');
INSERT INTO `pca_provinces` VALUES ('420000', '湖北省');
INSERT INTO `pca_provinces` VALUES ('430000', '湖南省');
INSERT INTO `pca_provinces` VALUES ('440000', '广东省');
INSERT INTO `pca_provinces` VALUES ('450000', '广西壮族自治区');
INSERT INTO `pca_provinces` VALUES ('460000', '海南省');
INSERT INTO `pca_provinces` VALUES ('500000', '重庆市');
INSERT INTO `pca_provinces` VALUES ('510000', '四川省');
INSERT INTO `pca_provinces` VALUES ('520000', '贵州省');
INSERT INTO `pca_provinces` VALUES ('530000', '云南省');
INSERT INTO `pca_provinces` VALUES ('540000', '西藏自治区');
INSERT INTO `pca_provinces` VALUES ('610000', '陕西省');
INSERT INTO `pca_provinces` VALUES ('620000', '甘肃省');
INSERT INTO `pca_provinces` VALUES ('630000', '青海省');
INSERT INTO `pca_provinces` VALUES ('640000', '宁夏回族自治区');
INSERT INTO `pca_provinces` VALUES ('650000', '新疆维吾尔自治区');
INSERT INTO `pca_provinces` VALUES ('710000', '台湾省');
INSERT INTO `pca_provinces` VALUES ('810000', '香港特别行政区');
INSERT INTO `pca_provinces` VALUES ('820000', '澳门特别行政区');

SET FOREIGN_KEY_CHECKS = 1;
