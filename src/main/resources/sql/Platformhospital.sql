/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : hospital_114

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 15/04/2022 10:19:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for hospital
-- ----------------------------
DROP TABLE IF EXISTS `platform_hospital`;
CREATE TABLE `platform_hospital`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '表唯一标识',
  `hospital_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '医院名称',
  `hospital_type` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '医院类型',
  `hospital_logo` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '医院logo url',
  `open_time` time(0) NOT NULL COMMENT '医院开放时间',
  `open_day` int NOT NULL COMMENT '开放多少天可预约',
  `release_time` time(0) NOT NULL COMMENT '放号时间',
  `stop_release_time` time(0) NOT NULL COMMENT '停止挂号时间',
  `withdrawal_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '退号时间显示',
  `area_id` int NOT NULL COMMENT '地区ID',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '医院描述',
  `address` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '详细地址',
  `traffic_routes` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '交通路线',
  `locationX` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '坐标X',
  `locationY` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '坐标Y',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = 'hospital' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of hospital
-- ----------------------------
INSERT INTO `platform_hospital` VALUES (1, '北京大学第一医院', '三级甲等', 'http://lk.vipgz4.91tunnel.com/114/images/hos_1.png', '08:00:00', 7, '16:00:00', '12:00:00', '就诊前一日12:00前取消', 110102, '北京大学第一医院（简称“北大医院”）位于北京老皇城内，是距离中南海最近的医院，是一所融医疗、教学、科研、预防为一体的大型综合性三级甲等医院，是中央保健基地医院。', '北京市西城区西什库大街8号', '乘坐公交13路、42路、68路、810路在厂桥路口西下车,步行至医院.', '1', '2');
INSERT INTO `platform_hospital` VALUES (2, '北京大学人民医院', '二级甲等', 'http://lk.vipgz4.91tunnel.com/114/images/hos_2.png', '10:00:00', 10, '16:00:00', '12:00:00', '就诊前一日12:00前取消', 110105, '北京大学第一医院（简称“北大医院”）位于北京老皇城内，是距离中南海最近的医院，是一所融医疗、教学、科研、预防为一体的大型综合性三级甲等医院，是中央保健基地医院。', '北京市西城区西什库大街8号', '乘坐公交13路、42路、68路、810路在厂桥路口西下车,步行至医院.', '4', '5');
INSERT INTO `platform_hospital` VALUES (3, '北京大学第三医院', '一级甲等', 'http://lk.vipgz4.91tunnel.com/114/images/hos_3.png', '11:00:00', 15, '16:00:00', '12:00:00', '就诊前一日12:00前取消', 110105, '北京大学第一医院（简称“北大医院”）位于北京老皇城内，是距离中南海最近的医院，是一所融医疗、教学、科研、预防为一体的大型综合性三级甲等医院，是中央保健基地医院。', '北京市西城区西什库大街8号', '乘坐公交13路、42路、68路、810路在厂桥路口西下车,步行至医院.', '4', '22');
INSERT INTO `platform_hospital` VALUES (4, '北京博爱医院', '三级甲等', 'http://lk.vipgz4.91tunnel.com/114/images/hos_4.png', '12:00:00', 28, '16:00:00', '12:00:00', '就诊前一日12:00前取消', 110102, '北京大学第一医院（简称“北大医院”）位于北京老皇城内，是距离中南海最近的医院，是一所融医疗、教学、科研、预防为一体的大型综合性三级甲等医院，是中央保健基地医院。', '北京市西城区西什库大街8号', '乘坐公交13路、42路、68路、810路在厂桥路口西下车,步行至医院.', '1', '99');

SET FOREIGN_KEY_CHECKS = 1;
