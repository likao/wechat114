loadMsg();

function loadMsg() {
    var openid = localStorage.getItem("openid");
    if (openid == null) {
        var code = location.href.split("&")[0].split("=")[1];
        if (code != null) {
            $.ajax({
                type: "post",
                url: targetUrl + "/wechat114/oAuth",
                data: {"code": code},
                async: false,
                dataType: "json",
                success: function (data) {
                    localStorage.setItem("openid", data.data);
                }
            })
        }
    }
}

//初始化医院信息
function loadHospital() {

    $.ajax({
        type: "post",
        url: targetUrl + "/wechat114/platIndex",
        async: false,
        dataType: "json",
        success: function (data) {
            var hospitals = data.data;
            for (var k in hospitals) {

                var releaseTime = dayjs(hospitals[k].releaseTime).format('HH:mm')

                $(".origin-list").append(" <a class=\"origin-item\" href=\"hospital.html?" + hospitals[k].id + "\">\n" +
                    "      <div class=\"origin-logo\">\n" +
                    "        <img src=\"" + hospitals[k].hospitalLogo + "\" />\n" +
                    "      </div>\n" +
                    "      <div class=\"origin-body\">\n" +
                    "        <h4>" + hospitals[k].hospitalName + "</h4>\n" +
                    "        <p>\n" +
                    "          <span>" + hospitals[k].hospitalType + "</span>\n" +
                    "          <span>每天" + releaseTime + "放号</span>\n" +
                    "        </p>\n" +
                    "      </div>\n" +
                    "    </a>");
            }

        }
    })

}
