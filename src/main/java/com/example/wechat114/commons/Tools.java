package com.example.wechat114.commons;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.*;

/**
 * @author CHENCHEN
 * @create 2021-03-29 14:54
 */
public class Tools {

    /**
     * 将请求中的xml转成map
     * @param request
     * @return
     * @throws IOException
     * @throws DocumentException
     */
    public static Map<String, String> xmlToMap(HttpServletRequest request) throws IOException, DocumentException {
        Map<String, String> map = new HashMap<>();
        SAXReader reader = new SAXReader();
        InputStream in = request.getInputStream();
        Document document = reader.read(in);
        Element root = document.getRootElement();
        List<Element> list = root.elements();
        for (Element e : list) {
            map.put(e.getName(), e.getText());
        }
        in.close();
        return map;
    }

    /**
     * 针对传入字符串进行字典排序
     * @param str1
     * @param str2
     * @param str3
     * @return
     */
    public static String sortStr(String str1, String str2, String str3){
        List<String> list = new ArrayList<>();
        list.add(str1);
        list.add(str2);
        list.add(str3);
        Collections.sort(list);
        String str = "";
        for (String s : list){
            str += s;
        }
        return str;
    }


    /**
     * 获取随机字符串
     * @return
     */
    public static String getNoncestr(){
        String str = UUID.randomUUID().toString().replaceAll("-", "");
        String noncestr = str.substring(0, 16);
        return noncestr;
    }

    /**
     * SHA1加密
     * @param inStr
     * @return
     */
    public static String getSha1Str(String inStr){
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA");
        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
            return "";
        }

        byte[] byteArray = new byte[0];
        try {
            byteArray = inStr.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte[] md5Bytes = sha.digest(byteArray);
        StringBuffer hexValue = new StringBuffer();
        for (int i = 0; i < md5Bytes.length; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16) {
                hexValue.append("0");
            }
            hexValue.append(Integer.toHexString(val));
        }
        return hexValue.toString();
    }
}
