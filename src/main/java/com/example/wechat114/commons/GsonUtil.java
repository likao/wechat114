package com.example.wechat114.commons;

import com.google.gson.Gson;

public class GsonUtil {
    private static Gson GsonUtilsGson = new Gson();

    /**
     * @fun 根据不同类型进行json到实体间的转化
     * @param jsonString json字符串
     * @param cls 需要转化的类型
     * @param <T> 需要转化的类型
     * @return 返回实体对象
     */
    public static  <T> T josnToModule(String jsonString, Class<T> cls) {
        T list ;
        list = GsonUtilsGson.fromJson(jsonString,cls);
        return list;
    }



}
