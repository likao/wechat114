package com.example.wechat114.commons;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @author CHENCHEN
 * @create 2021-03-29 15:03
 */
public class CreateMenu {
    public static void main(String[] args) {
        String url = WechatAPI.CREATE_MENU_URL.replace("ACCESS_TOKEN", Constant.getTemporaryAccessToken());
        //发送的json数据
        JSONObject json = new JSONObject();
        //所有按钮组数据
        JSONArray button = new JSONArray();
        //第一个一级菜单
        JSONObject firstMenu = new JSONObject();
        firstMenu.put("type", "view");
        firstMenu.put("name", "核酸检测");
        firstMenu.put("url", Constant.oAuth(Constant.URL + "/114/hsjc.html", Constant.SNSAPI_USERINFO));

        //第二个一级菜单
        JSONObject secondMenu = new JSONObject();
        secondMenu.put("name", "就医服务");

        //第二个一级菜单的第一个二级菜单
        JSONObject secondMenuInFirstMenu = new JSONObject();
        secondMenuInFirstMenu.put("type", "view");
        secondMenuInFirstMenu.put("name", "预约挂号");
        secondMenuInFirstMenu.put("url", Constant.oAuth(Constant.URL + "/114/index.html", Constant.SNSAPI_USERINFO));

        //第二个一级菜单的第二个二级菜单
        JSONObject secondMenuInSecondMenu = new JSONObject();
        secondMenuInSecondMenu.put("type", "view");
        secondMenuInSecondMenu.put("name", "个人中心");
        secondMenuInSecondMenu.put("url", Constant.oAuth(Constant.URL + "/114/center.html", Constant.SNSAPI_USERINFO));

        //第二个一级菜单的第三个二级菜单
        JSONObject secondMenuInThirdMenu = new JSONObject();
        secondMenuInThirdMenu.put("type", "view");
        secondMenuInThirdMenu.put("name", "下载APP");
        secondMenuInThirdMenu.put("url", Constant.oAuth(Constant.URL + "/114/xiazai.html", Constant.SNSAPI_USERINFO));

        //第二个一级菜单的所有按钮组数据
        JSONArray secondMenuTowMenus = new JSONArray();
        secondMenuTowMenus.put(secondMenuInFirstMenu);
        secondMenuTowMenus.put(secondMenuInSecondMenu);
        secondMenuTowMenus.put(secondMenuInThirdMenu);
        secondMenu.put("sub_button", secondMenuTowMenus);

        //第三个一级菜单
        JSONObject thirdMenu = new JSONObject();
        thirdMenu.put("type", "view");
        thirdMenu.put("name", "关于我们");
        thirdMenu.put("url", Constant.oAuth(Constant.URL + "/114/more.html", Constant.SNSAPI_USERINFO));

        //所有按钮组
        button.put(firstMenu);
        button.put(secondMenu);
        button.put(thirdMenu);
        json.put("button", button);

        String s = HttpClientUtil.doPost(url, json.toString());
        System.out.println(s);
    }
}
