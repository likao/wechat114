package com.example.wechat114.service.impl;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import com.example.wechat114.mapper.PlatformDepartmentMapper;
import com.example.wechat114.entity.PlatformDepartment;
import com.example.wechat114.service.PlatformDepartmentService;

import java.util.List;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 15:54 2022/4/16
 * @Modified By:
 */
@Service
public class PlatformDepartmentServiceImpl implements PlatformDepartmentService {

    @Resource
    private PlatformDepartmentMapper platformDepartmentMapper;

    @Override
    public int insert(PlatformDepartment record) {
        return platformDepartmentMapper.insert(record);
    }

    @Override
    public int insertSelective(PlatformDepartment record) {
        return platformDepartmentMapper.insertSelective(record);
    }

    @Override
    public List<PlatformDepartment> selectMainDept() {
        return platformDepartmentMapper.selectMainDept();
    }

    @Override
    public List<PlatformDepartment> selectSecondDept(Integer deptId) {
        return platformDepartmentMapper.selectSecondDept(deptId);
    }

    @Override
    public List<PlatformDepartment> selectAllHospitalByDept(Integer deptId) {
        return platformDepartmentMapper.selectAllHospitalByDept(deptId);
    }

    @Override
    public List<PlatformDepartment> selectAllDept() {
        return platformDepartmentMapper.selectAllDept();
    }

}
