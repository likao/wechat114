package com.example.wechat114.service.impl;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.example.wechat114.entity.Department;
import com.example.wechat114.mapper.DepartmentMapper;
import com.example.wechat114.service.DepartmentService;

import java.util.List;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 14:29 2022/4/15
 * @Modified By:
 */
@Service
@CacheConfig(cacheManager = "SimpleCacheManager",cacheNames = "hospitalCache")
public class DepartmentServiceImpl implements DepartmentService{

    @Resource
    private DepartmentMapper departmentMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return departmentMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Department record) {
        return departmentMapper.insert(record);
    }

    @Override
    public int insertSelective(Department record) {
        return departmentMapper.insertSelective(record);
    }

    @Override
    public Department selectByPrimaryKey(Integer id) {
        return departmentMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Department record) {
        return departmentMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Department record) {
        return departmentMapper.updateByPrimaryKey(record);
    }

    @Override
    @Cacheable(keyGenerator ="department")
    public List<Department> selectMainDept() {
        return departmentMapper.selectMainDept();
    }

    @Override
    @Cacheable(keyGenerator ="department")
    public List<Department> selectSecondDept(Integer deptId) {
        return departmentMapper.selectSecondDept(deptId);
    }

}
