package com.example.wechat114.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.example.wechat114.entity.Hospital;
import com.example.wechat114.mapper.HospitalMapper;
import com.example.wechat114.service.HospitalService;

import java.util.List;

/**
 * @Atuthor:Likao
 * @Description:
 * @Date:Created in 10:12 2022-04-15
 * @Modified By:
 */
@Service
@CacheConfig(cacheManager = "SimpleCacheManager",cacheNames = "hospitalCache")
public class HospitalServiceImpl implements HospitalService{

    private static final String HOSPITAL_KEY="hospitals";

    @Autowired
    private RedisTemplate redisTemplate;

    @Resource
    private HospitalMapper hospitalMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return hospitalMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(Hospital record) {
        return hospitalMapper.insert(record);
    }

    @Override
    public int insertSelective(Hospital record) {
        return hospitalMapper.insertSelective(record);
    }

    @Override
    @Cacheable(keyGenerator = "hospital")
    public Hospital selectByPrimaryKey(Integer id) {
        return hospitalMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(Hospital record) {
        return hospitalMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(Hospital record) {
        return hospitalMapper.updateByPrimaryKey(record);
    }

    @Override
    @Cacheable(keyGenerator = "hospital")
    public List<Hospital> selectAllHospital() {

//        List<Hospital> hospitals=null;
//        BoundValueOperations operation = redisTemplate.boundValueOps(HOSPITAL_KEY);
//        if (operation==null||operation.size()<=0){
//            hospitals = hospitalMapper.selectAllHospital();
//            operation.set(hospitals);
//        }else {
//            hospitals= (List<Hospital>) operation.get();
//        }
          List<Hospital> hospitals = hospitalMapper.selectAllHospital();
        return hospitals;
    }

}
