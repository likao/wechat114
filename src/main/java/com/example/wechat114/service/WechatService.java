package com.example.wechat114.service;

import com.example.wechat114.commons.JsonBean;

import java.util.Map;

/**
 * @author CHENCHEN
 * @create 2021-03-29 14:45
 */
public interface WechatService {

    String handleWxMsg(Map<String, String> map);

    JsonBean oAuth(String code);

    JsonBean jsConfig(String url);
}
