package com.example.wechat114.service;

import com.example.wechat114.entity.Hospital;

import java.util.List;

/**
 * @Atuthor:Likao
 * @Description:
 * @Date:Created in 10:12 2022-04-15
 * @Modified By:
 */
public interface HospitalService{


    int deleteByPrimaryKey(Integer id);

    int insert(Hospital record);

    int insertSelective(Hospital record);

    Hospital selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Hospital record);

    int updateByPrimaryKey(Hospital record);

    List<Hospital>selectAllHospital();

}
