package com.example.wechat114.service;

import com.example.wechat114.entity.PlatformDepartment;

import java.util.List;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 15:54 2022/4/16
 * @Modified By:
 */
public interface PlatformDepartmentService {


    int insert(PlatformDepartment record);

    int insertSelective(PlatformDepartment record);

    List<PlatformDepartment> selectMainDept();

    List<PlatformDepartment> selectSecondDept(Integer deptId);

    List<PlatformDepartment> selectAllHospitalByDept(Integer deptId);

    List<PlatformDepartment> selectAllDept();
}
