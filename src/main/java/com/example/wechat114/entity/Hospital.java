package com.example.wechat114.entity;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Atuthor:Likao
 * @Description:
 * @Date:Created in 10:12 2022-04-15
 * @Modified By:
 */
/**
    * hospital
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hospital implements Serializable {
    /**
    * 表唯一标识
    */
    private Integer id;

    /**
    * 医院名称
    */
    private String hospitalName;

    /**
    * 医院类型
    */
    private String hospitalType;

    /**
    * 医院logo url
    */
    private String hospitalLogo;

    /**
    * 医院开放时间
    */
    private Date openTime;

    /**
    * 开放多少天可预约
    */
    private Integer openDay;

    /**
    * 放号时间
    */
    private Date releaseTime;

    /**
    * 停止挂号时间
    */
    private Date stopReleaseTime;

    /**
    * 退号时间显示
    */
    private String withdrawalTime;

    /**
    * 地区ID
    */
    private Integer areaId;

    /**
    * 医院描述
    */
    private String description;

    /**
    * 详细地址
    */
    private String address;

    /**
    * 交通路线
    */
    private String trafficRoutes;

    /**
    * 坐标X
    */
    private String locationx;

    /**
    * 坐标Y
    */
    private String locationy;

    private static final long serialVersionUID = 1L;

}