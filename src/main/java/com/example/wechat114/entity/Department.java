package com.example.wechat114.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 14:29 2022/4/15
 * @Modified By:
 */
/**
    * department
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department implements Serializable {
    /**
    * 科室表唯一标识
    */
    private Integer id;

    /**
    * 科室名称
    */
    private String deptName;

    /**
    * 科室状态0：禁用，1：启用
    */
    private Byte status;

    /**
    * 上级科室id
    */
    private Integer preDepartId;

    private static final long serialVersionUID = 1L;
}