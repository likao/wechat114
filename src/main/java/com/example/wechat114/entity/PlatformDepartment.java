package com.example.wechat114.entity;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 15:54 2022/4/16
 * @Modified By:
 */
/**
    * platform_department
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlatformDepartment implements Serializable {
    /**
    * 科室表唯一标识
    */
    private Integer id;

    /**
    * 科室名称
    */
    private String deptName;

    /**
    * 科室状态0：禁用，1：启用
    */
    private Byte status;

    /**
    * 上级科室id
    */
    private Integer preDepartId;

    /**
    * 医院ID
    */
    private Integer hospitalId;

    private static final long serialVersionUID = 1L;
}