package com.example.wechat114;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.example.wechat114.mapper")
@EnableCaching
public class Wechat114Application {

    public static void main(String[] args) {
        SpringApplication.run(Wechat114Application.class, args);
    }

}
