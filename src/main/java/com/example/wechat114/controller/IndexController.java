package com.example.wechat114.controller;

import com.example.wechat114.commons.JsonBean;
import com.example.wechat114.entity.Hospital;
import com.example.wechat114.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Atuthor:Likao
 * @Description: 用于首页
 * @Date:Created in 10:24 2022-04-15
 * @Modified By:
 */
@RestController()
public class IndexController {

    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private RedisTemplate redisTemplate;




    @RequestMapping("platIndex")
    public JsonBean index() {

        JsonBean jsonBean = new JsonBean(0, "ok", "");

        List<Hospital> hospitals = hospitalService.selectAllHospital();

        jsonBean.setCode(1);
        jsonBean.setData(hospitals);
        return jsonBean;
    }
    @RequestMapping("platIndex/{id}")
    public JsonBean getHospitalById(@PathVariable("id") Integer id) {

        JsonBean jsonBean = new JsonBean(0, "ok", "");

        Hospital hospital = hospitalService.selectByPrimaryKey(id);

        jsonBean.setCode(1);
        jsonBean.setData(hospital);
        return jsonBean;
    }


}
