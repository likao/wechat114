package com.example.wechat114.controller;

import com.example.wechat114.commons.JsonBean;
import com.example.wechat114.entity.PlatformDepartment;
import com.example.wechat114.service.HospitalService;
import com.example.wechat114.service.PlatformDepartmentService;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 15:55 2022/4/16
 * @Modified By:
 */
@RestController
public class DepartmentController {
    @Autowired
    private PlatformDepartmentService platformDepartmentService;

    @Autowired
    private HospitalService hospitalService;
    ;

    @GetMapping("getAllMainDepartment")
    public JsonBean getMainDepartment() {
        JsonBean jsonBean = new JsonBean();
        List<PlatformDepartment> deptList = platformDepartmentService.selectMainDept();
        if (deptList != null) {
            jsonBean.setCode(1);
            jsonBean.setData(deptList);
        } else
            jsonBean.setCode(0);
        return jsonBean;
    }

    @GetMapping("getAllSecondDepartment")
    public JsonBean getAllSecondDepartment(Integer deptId) {
        JsonBean jsonBean = new JsonBean();
        List<PlatformDepartment> deptList = platformDepartmentService.selectSecondDept(deptId);
        if (deptList != null) {
            for (PlatformDepartment platformDepartment : deptList) {
                System.out.println(platformDepartment);
            }
            jsonBean.setCode(1);
            jsonBean.setData(deptList);
        } else
            jsonBean.setCode(0);
        return jsonBean;
    }

    //    @GetMapping("getHospitalByDept")
//    public JsonBean getHospitalByDept(Integer deptId) {
//        JsonBean jsonBean = new JsonBean();
//        List<PlatformDepartment> deptList = platformDepartmentService.selectAllHospitalByDept(deptId);
//    }
    @GetMapping("selectAllDepartment")
    public JsonBean selectAllDepartment() {
        JsonBean jsonBean = new JsonBean();
        List<PlatformDepartment> deptList = platformDepartmentService.selectAllDept();
        if (deptList != null) {
            for (PlatformDepartment platformDepartment : deptList) {
                System.out.println(platformDepartment);
            }
            jsonBean.setCode(1);
            jsonBean.setData(deptList);
        } else
            jsonBean.setCode(0);
        return jsonBean;
    }
}
