package com.example.wechat114.controller;


/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 15:10 2022/4/15
 * @Modified By:
 */

import com.example.wechat114.commons.JsonBean;
import com.example.wechat114.entity.Department;
import com.example.wechat114.entity.Hospital;
import com.example.wechat114.service.DepartmentService;
import com.example.wechat114.service.HospitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HospitalIndexController {
    @Autowired
    private HospitalService hospitalService;

    @Autowired
    private DepartmentService departmentService;

    /**
     * 获取医院信息
     *
     * @param hosId
     * @return
     */
    @GetMapping("getHospitalInfo")
    public JsonBean getHospitalInfo(Integer hosId) {
        JsonBean jsonBean = new JsonBean();
        Hospital hospital = hospitalService.selectByPrimaryKey(hosId);
        if (hospital != null) {
            jsonBean.setCode(1);
            jsonBean.setData(hospital);
        } else
            jsonBean.setCode(0);

        return jsonBean;
    }

    /**
     * 获取一级科室
     *
     * @return
     */
    @GetMapping("getMainDepartment")
    public JsonBean getDepartmentInfo() {
        JsonBean jsonBean = new JsonBean();
        List<Department> departmentList = departmentService.selectMainDept();
        if (departmentList != null) {
            jsonBean.setCode(1);
            jsonBean.setData(departmentList);
            System.out.println(departmentList);
        } else
            jsonBean.setCode(0);
        return jsonBean;
    }

    /**
     * 获取二级科室
     *
     * @param deptId
     * @return
     */
    @GetMapping("getSecondDepartment")
    public JsonBean getSecondDepartment(Integer deptId) {
        JsonBean jsonBean = new JsonBean();
        List<Department> departmentList = departmentService.selectSecondDept(deptId);
        if (departmentList != null) {
            jsonBean.setCode(1);
            jsonBean.setData(departmentList);
        } else {
            jsonBean.setCode(0);
        }
        return jsonBean;
    }

}
