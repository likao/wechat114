package com.example.wechat114.mapper;

import com.example.wechat114.entity.Department;

import java.util.List;

/**
 * @Atuthor:WangYunLong
 * @Description:
 * @Date:Created in 14:29 2022/4/15
 * @Modified By:
 */
public interface DepartmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Department record);

    int insertSelective(Department record);

    Department selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Department record);

    int updateByPrimaryKey(Department record);

    List<Department> selectMainDept();

    List<Department> selectSecondDept(Integer deptId);
}