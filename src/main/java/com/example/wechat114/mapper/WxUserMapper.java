package com.example.wechat114.mapper;

import com.example.wechat114.entity.WxUser;

public interface WxUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(WxUser record);

    int insertSelective(WxUser record);

    WxUser selectByPrimaryKey(Integer id);

    WxUser selectByOpenid(String openid);

    int updateByPrimaryKeyOpenid(WxUser record);

    int updateByPrimaryKey(WxUser record);
}