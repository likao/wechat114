package com.example.wechat114.config;

/**
 * @Atuthor:Likao
 * @Description:
 * @Date:Created in 20:07 2022-04-15
 * @Modified By:
 */
public class RedisCacheKey {

    public static final String HOSPITAL_PRE="hospital";
    public static final String DEPART_PRE="department";
}
